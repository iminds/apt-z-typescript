const { dependencies, devDependencies } = require("./dependencies");
const { spawn } = require("child_process");
const pEachSeries = require("p-each-series");

const pms = {
  YARN: "yarn",
  NPM: "npm"
};

function executeCommand({ command, cwd, chalk }) {
  return new Promise((resolve, reject) => {
    console.log(chalk.blue(`Execute: ${command.join(" ")}`));
    const ls = spawn(command.shift(), command, { cwd });

    ls.stdout.setEncoding("utf8");
    ls.stdout.on("data", data => {
      console.log(data);
    });

    ls.stderr.setEncoding("utf8");
    ls.stderr.on("data", data => {
      console.error(chalk.red(data));
    });

    ls.on("close", code => {
      const color = code === 0 ? chalk.green : chalk.red;
      console.log(color(`child process exited with code ${code}`));
      resolve();
    });
  });
}
module.exports = {
  enforceNewFolder: true,
  prompts: {
    app: {
      type: "input",
      message: "Application/repo name (in kebab case, please!):"
    },
    user: {
      type: "input",
      message: "Your (full) name:",
      store: true
    },
    email: {
      type: "input",
      message: "Your e-mail address:",
      store: true
    },
    license: {
      type: "list",
      message: "License:",
      choices: ["Proprietary", "MIT", "GPL-v3"],
      default: "Proprietary"
    },
    packageManager: {
      type: "list",
      message: "Which package manager should be used",
      choices: [pms.YARN, pms.NPM],
      store: true,
      default: pms.YARN
    }
  },
  data(answers) {
    return Object.assign({}, dependencies);
  },
  post({ answers, folderName, chalk }) {
    const pm = answers.packageManager;
    let commands = [["git", "init"]]; // yeah, we want an array of arrays
    switch (pm) {
      case pms.YARN:
        if (dependencies && dependencies.length) {
          commands.push([pm, "add", ...dependencies]);
        }
        if (devDependencies && devDependencies.length) {
          commands.push([pm, "add", "-D", ...devDependencies]);
        }
        break;
      case pms.NPM:
        if (dependencies && dependencies.length) {
          commands.push([pm, "install", "-S", ...dependencies]);
        }
        if (devDependencies && devDependencies.length) {
          commands.push([pm, "install", "-D", ...devDependencies]);
        }
        break;
    }
    commands.push(
      ["git", "add", "-A"],
      [
        "git",
        "commit",
        "-m",
        "build(app): initial scaffolding with apt-z-typescript"
      ]
    );

    const opts = {
      cwd: folderName,
      chalk
    };
    pEachSeries(commands, command => executeCommand({ command, ...opts }));
  }
};
