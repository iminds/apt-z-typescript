module.exports = {
    dependencies:[],
    devDependencies: [
        "@commitlint/cli",
        "@commitlint/config-angular",
        "@types/jest",
        "@types/node",
        "conventional-changelog-cli",
        "husky",
        "jest",
        "nodemon",
        "npm-run-all",
        "prettier",
        "pretty-quick",
        "ts-jest",
        "ts-node",
        "tsconfig-paths",
        "tslint",
        "tslint-config-prettier",
        "typescript"
    ]
}
