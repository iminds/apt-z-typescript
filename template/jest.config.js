module.exports = {
  collectCoverageFrom: ["src/**/*.{ts,tsx,js,jsx}", "!src/**/*.d.ts"],
  coverageDirectory: "coverage",
  globals: {
    "ts-jest": {
      skipBabel: true,
      tsConfigFile: "tsconfig.json"
    }
  },
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  moduleNameMapper: {
    "^~/(.*)$": "<rootDir>/src/$1"
  },
  testPathIgnorePatterns: ["/src/server/tests", "/production-server/"],
  testEnvironment: "node",
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(ts|js)x?$",
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  }
};
