# APT Z project base for typescript

## Requirements

Make sure `sao` is installed globally:

```
$ yarn global add sao
```

Or - if you REALLY must:

```
$ npm install -g sao
```

## Usage

Then use it to scaffold a new project:

```
$ sao bitbucket:iminds/apt-z-typescript -u ./my-project-name
```

(`-u` will update the template to the latest version)

This will create a project using the files in `apt-z-typescript/template` as a base and using your answers on the questions asked.

## Development

* See [sao.js](https://sao.js.org/) for docs
* If you want to use the local template you're working on you can run `$ sao ../path/to/template`
* If you want to use the template in a specific branch run:

    ```
    $ sao bitbucket:iminds/apt-z-typescript#branch-name ./my-project-name
    ```
